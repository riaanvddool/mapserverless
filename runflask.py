
from flask import Flask, Response, request
from handler import mapserv
import os

PORT = os.environ.get('PORT', 5000)

app = Flask(__name__)
app.url_map.strict_slashes = False

@app.route('/')
def mapserver():
    http_method = request.method
    queryparams = request.args
    headers = request.headers
    data = request.data
    remote_addr = request.remote_addr
    
    result = mapserv(http_method, queryparams, headers, data, remote_addr)

    response = Response(result.get('body', ''), mimetype=result.get('Content-Type'))
    for key, value in result.get('headers').items():
        response.headers[key] = value
    return response

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=int(PORT))