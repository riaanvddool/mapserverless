# Adopted from https://github.com/dschep/serverless-cgi
import os
from subprocess import check_output
from urllib.parse import urlencode
import base64
from pathlib import Path


def mapserv_handler(event, context):
    http_method = event.get('httpMethod')
    queryparams=event.get('queryStringParameters', {})
    headers = event.get('headers', {})
    data = event['body']
    remote_addr = event['requestContext']['identity']['sourceIp']
    return mapserv(http_method, queryparams, headers, data, remote_addr)

    
def mapserv(http_method, queryparams, headers, data, remote_addr):

    # Dependencies path prefix
    DEP_PREFIX = os.environ.get('DEP_PREFIX', '/usr/local/')

    env = os.environ.copy()
    env.update(**{'HTTP_' + name.upper().replace("-", "_"): value for name, value in headers.items()})

    # script = event['pathParameters']['path']
    env.update(
        MS_MAPFILE='map.map',
        MS_MAP_NO_PATH='nomap',
        MS_TEMPPATH='/tmp/',
        MS_OPENLAYERS_JS_URL='https://www.mapserver.org/lib/OpenLayers-ms60.js',
        SERVER_SOFTWARE='serverless-cgi/0.1',
        SERVER_NAME=headers.get('Host', ''),
        GATEWAY_INTERFACE='CGI/1.1',
        SERVER_PROTOCOL='HTTP/1.1',
        SERVER_PORT=headers.get('X-Forwarded-Port', '' ),
        REQUEST_METHOD=http_method,
        PATH_INFO='',  # TODO
        PATH_TRANSLATED='',  # TODO
        SCRIPT_NAME='',  # TODO
        QUERY_STRING=urlencode(queryparams),
        REMOTE_ADDR=remote_addr,
        CONTENT_TYPE=headers.get('Content-Type'),
        CONTENT_LENGTH=(len(data) if data is not None else 0),
        PROJ_LIB="{}/share/proj".format(DEP_PREFIX)
    )
    for key, value in env.items():
        try:
            os.environ[key]=value
        except:
            pass

    # Reponse headers and body:
    if not queryparams:
        with open('ol.html', 'r') as file:
            body = file.read().replace('<server>',env['SERVER_NAME'])
            return {
                "statusCode": 200,
                "body": body,
                "headers": {'Content-Type': 'text/html'}
            }

    # call script
    cgi_header, cgi_body = check_output(['mapserv']).split(b'\r\n\r\n',1)
    headers={}
    
    # cgi_header contains content-type
    cgi_header_key, cgi_header_value = cgi_header.decode().split(': ',1)
    headers[cgi_header_key] = cgi_header_value

    # CORS headers
    headers['Access-Control-Allow-Origin']='*'
    headers['Access-Control-Allow-Headers']='Content-Type'
    headers['Access-Control-Allow-Methods']='OPTIONS,GET'

    if cgi_header_value == 'image/png':
        # body = base64.encodebytes(cgi_body).decode()
        body = cgi_body
        b64 = "true"
    else:
        body = cgi_body.decode()
        b64 = "false"

    response = {
        "statusCode": 200,
        "body": body,
        "headers": headers,
        "isBase64Encoded": b64,
        "Content-Type": cgi_header_value,
    }

    return response

