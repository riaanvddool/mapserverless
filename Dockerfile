FROM lambci/lambda:build-provided

# install system libraries
RUN \
    yum makecache fast; \
    yum install -y python34-pip; \
    yum clean all; \
    yum autoremove

RUN ln -s /usr/bin/pip-3.4 /usr/local/bin/pip3

# PIP install Python modules
COPY ./requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt

ENV DEP_PREFIX /usr/local

ADD layer/mapserverless-layer.tar.gz ${DEP_PREFIX}

WORKDIR /home/mapserverless

# Copy source files
COPY . src

WORKDIR src

# Define default command
CMD ["python3", "runflask.py"]

# Service must listen to $PORT environment variable.
# This default value facilitates local development.
ENV PORT 8055

# Expose TCP ports
EXPOSE 8055

